FROM debian:bookworm-slim
LABEL maintainer "Yann MASSON"


ENV container=docker
ENV DEBIAN_FRONTEND=noninteractive

ADD Gemfile Gemfile
ADD requirements.txt requirements.txt

RUN echo 'APT::Install-Recommends "0";' >/etc/apt/apt.conf.d/01norecommend && \
    echo 'APT::Install-Suggests "0";' >>/etc/apt/apt.conf.d/01norecommend && \
    apt-get update && \
    apt-get install --no-install-recommends -y \
      build-essential \
      bundler \
      ca-certificates \
      curl \
      git \
      gnupg2 \
      net-tools \
      openssh-client \
      python3-neutronclient \
      python3-openstackclient \
      python3-pip \
      ruby \
      ruby-dev \
      zlib1g-dev \
      && \
    curl -fsSL https://download.docker.com/linux/debian/gpg -o - |gpg --dearmor >/etc/apt/keyrings/docker.gpg && \
    echo "deb [signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian bookworm stable" > /etc/apt/sources.list.d/docker.list && \
    apt-get update && \
    apt-get install --no-install-recommends -y \
      docker-ce \
      docker-ce-cli \
      containerd.io \
      docker-buildx-plugin \
      && \
    bundle && rm -f Gemfile Gemfile.lock && \
    pip3 install --upgrade --break-system-packages -r requirements.txt && \
    echo "    StrictHostKeyChecking no" >>/etc/ssh/ssh_config && \
    rm -f requirements.txt ; \
    apt-get remove --purge -y zlib1g-dev ruby-dev build-essential && \
    apt-get install --no-install-recommends ruby zlib1g && \
    apt-get --purge autoremove -y && apt-get autoclean -y && apt-get clean -y && \
    rm -rf /usr/share/{doc,icons,poppler,locale} && \
    rm -rf /var/cache/debconf/* && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* /var/tmp/*

# Patch Kithen username variable for shell verifier
# https://github.com/test-kitchen/test-kitchen/issues/1399
RUN find / -name 'shell.rb' |grep verifier |xargs sed -i 's/        state.each_pair/        env_state[:environment]["KITCHEN_USERNAME"] = instance.transport[:username]\n        state.each_pair/'

# install kitchen common configuration
COPY kitchen /opt/kitchen
