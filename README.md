# Pipeline
Devel: [![pipeline status](https://gitlab.com/ymasson-salt/ci-kitchen/badges/devel/pipeline.svg)](https://gitlab.com/ymasson-salt/ci-kitchen/commits/devel)


Master: [![pipeline status](https://gitlab.com/ymasson-salt/ci-kitchen/badges/master/pipeline.svg)](https://gitlab.com/ymasson-salt/ci-kitchen/commits/master)

# Description
KitchenCI image. https://kitchen.ci/

## Kitchen driver
* Docker
* Openstack
* vagrant

## Kitchen verifier
TestInfra. https://testinfra.readthedocs.io/
