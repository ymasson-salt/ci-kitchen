#!/bin/bash


MAX_INSTANCES=8


# check environment
if [ -z "${OS_AUTH_URL}" ] || [ -z "${OS_REGION_NAME}" ] || \
       [ -z "${OS_PROJECT_ID}" ] || [ -z "${OS_PASSWORD}" ] || [ -z "${OS_USERNAME}" ]; then
    echo "
Please define following environment variables:
  OS_AUTH_URL             (example: https://api-cloud.fr:5000/v3 ==> v3 API is required)
  OS_REGION_NAME
  OS_PROJECT_ID
  OS_PASSWORD
  OS_USERNAME
"
    exit 1
fi


# create env file in .env
ENV_FILE=".env/$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w10 | head -1)"
mkdir -p $(dirname ${ENV_FILE})

# use v3
export OS_IDENTITY_API_VERSION=3
export OS_INTERFACE=public
export OS_TENANT_ID=${OS_PROJECT_ID}
export KITCHEN_SSH_KEY_NAME=""

# create key and publish
TOKEN_OK=""
[ ! -f ${HOME}/.ssh/id_rsa ] && ssh-keygen  -N '' -f ${HOME}/.ssh/id_rsa
while [ -z "${TOKEN_OK}" ]; do
  for i in $(seq 1 ${MAX_INSTANCES}); do
    KITCHEN_SSH_KEY_NAME="kitchen-${i}"
    openstack keypair create --public-key ${HOME}/.ssh/id_rsa.pub ${KITCHEN_SSH_KEY_NAME}
    [ $? -eq 0 ] && TOKEN_OK="ok" && break
  done
    if [ -z "${TOKEN_OK}" ]; then
      echo "A CI job is already running on this tenant, I'm waiting 20 seconds..."
      sleep 20
    fi
done
echo "KITCHEN_SSH_KEY_NAME=${KITCHEN_SSH_KEY_NAME}" >> ${ENV_FILE}

# get openstack environment
while true; do
  export KITCHEN_FLAVOR=$(openstack flavor list -c Name -c RAM -f value  | sort -n -k 2,3 | head -1 | awk '{print $1}')
  export KITCHEN_NETWORK_PUBLIC=$(openstack network list -f value -c Name | head -1)
  [ ! -z "${KITCHEN_FLAVOR}" ] && [ ! -z "${KITCHEN_NETWORK_PUBLIC}" ] && break
  sleep 20
done

# server name prefix
export KITCHEN_SERVER_PREFIX="kitchen-${CI_JOB_NAME}"
echo "KITCHEN_SERVER_PREFIX=${KITCHEN_SERVER_PREFIX}" >> ${ENV_FILE}


# run kitchen tests
[ ! -f default.yml ] && cp /opt/kitchen/kitchen-default-playbook.yml default.yml
export KITCHEN_YAML="/opt/kitchen/kitchen.openstack.yml"
kitchen test -c 1 $@
RETCODE=$?

# cleanup phase and exit
# - instance
# - ssh key
kitchen destroy $@
export LANG=C
[ ! -z "${KITCHEN_SERVER_PREFIX}" ] && openstack server list -f value -c Name | grep "${KITCHEN_SERVER_PREFIX}"  | xargs -i openstack server delete {}
[ ! -z "${KITCHEN_SSH_KEY_NAME}" ] && openstack keypair delete ${KITCHEN_SSH_KEY_NAME}

exit ${RETCODE}
