#!/bin/bash

# chek environment
if [ ! -d /tmp/role-to-test ] && [ -z "${CI_PROJECT_DIR}" ]; then
    echo "Map the repository folder of the role to /tmp/role-to-test"
    echo '   docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):/tmp/role-to-test $0 <image>'
    exit 1
fi
if [ ! -e /var/run/docker.sock ] && [ -z "${DOCKER_HOST}" ]; then
    echo "Share host docker socket: unix or tcp with DOCKER_HOST"
    echo '   docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock -v $(pwd):/tmp/role-to-test $0 <image>'
    exit 1
fi

# fix: DinD, use the gateway IP to SSH kitchen-docker created instances
# -> need to patch ruby code for now :(
host=$(route | grep default | awk '{print $2}')
find /var/lib -name "container.rb" -exec sed -i "s/hostname = 'localhost'/hostname = '${host}'/g" {} \;


# If folder exists, assume test are run in local copy
# => create a copy inside the docker
# => workdir become the new copy
if [ -d /tmp/role-to-test ]; then
    cp -R /tmp/role-to-test /tmp/workdir
    cd /tmp/workdir
fi

[ ! -f default.yml ] && cp /opt/kitchen/kitchen-default-playbook.yml default.yml
export KITCHEN_YAML="/opt/kitchen/kitchen.docker.yml"

if [ "$(basename $0)" = "docker-run-tests.sh" ]; then
  # do not run testinfra tests (service are not started)
  kitchen setup $1
  RETCODE=$?
else
  kitchen test $@
  RETCODE=$?
fi

# cleanup and exit
kitchen destroy $1
exit ${RETCODE}
